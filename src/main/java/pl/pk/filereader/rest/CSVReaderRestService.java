package pl.pk.filereader.rest;

import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import org.springframework.beans.factory.annotation.Value;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class CSVReaderRestService {

    @Value("${file.path}")
    private String filePath;

    public void readDataLineByLine(String file) {

        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(file), ';');
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        ColumnPositionMappingStrategy<Shop> beanStrategy = new ColumnPositionMappingStrategy<Shop>();
        beanStrategy.setType(Shop.class);
        beanStrategy.setColumnMapping("ownerName", "shopName", "city", "street", "buildingNbr", "type", "premiseNbr", "adressDescription");

        CsvToBean<Shop> csvToBean = new CsvToBean<Shop>();

        List<Shop> emps = csvToBean.parse(beanStrategy, reader);

        for (Shop shop : emps) {
            System.out.println(shop);
        }
    }
}
