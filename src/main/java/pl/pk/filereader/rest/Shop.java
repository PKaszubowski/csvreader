package pl.pk.filereader.rest;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
@Table
public class Shop {


    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ownerName")
    private String ownerName;
    @Column(name = "shopName")
    private String shopName;
    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @Column(name = "buildingNbr")
    private String buildingNbr;
    @Column(name = "type")
    private String type;
    @Column(name = "premiseNbr")
    private String premiseNbr;
    @Column(name = "adressDescription")
    private String adressDescription;


    public Shop() {
    }

    public Shop(String ownerName, String shopName, String city, String street, String buildingNbr, String type, String premiseNbr, String adressDescription) {
        this.ownerName = ownerName;
        this.shopName = shopName;
        this.city = city;
        this.street = street;
        this.buildingNbr = buildingNbr;
        this.type = type;
        this.premiseNbr = premiseNbr;
        this.adressDescription = adressDescription;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuildingNbr() {
        return buildingNbr;
    }

    public void setBuildingNbr(String buildingNbr) {
        this.buildingNbr = buildingNbr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPremiseNbr() {
        return premiseNbr;
    }

    public void setPremiseNbr(String premiseNbr) {
        this.premiseNbr = premiseNbr;
    }

    public String getAdressDescription() {
        return adressDescription;
    }

    public void setAdressDescription(String adressDescription) {
        this.adressDescription = adressDescription;
    }


    @Override
    public String toString() {
        return "Shop{" +
                "ownerName='" + ownerName + '\'' +
                ", shopName='" + shopName + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", buildingNbr='" + buildingNbr + '\'' +
                ", type='" + type + '\'' +
                ", premiseNbr='" + premiseNbr + '\'' +
                ", adressDescription='" + adressDescription + '\'' +
                '}';

    }
}


